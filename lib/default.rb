# All files in the 'lib' directory will be loaded
# before nanoc starts compiling.
#http://clarkdave.net/2012/02/building-a-static-blog-with-nanoc/

include Nanoc3::Helpers::Blogging
include Nanoc3::Helpers::Tagging
include Nanoc3::Helpers::Rendering
include Nanoc3::Helpers::LinkTo

module PostHelper
	def get_post_start(post)
		content = post.compiled_content
		if content =~ /\s<!-- more -->\s/
			content = content.partition('<!-- more -->').first + "<div class='read-more'><a href='#{post.path}'>Continue reading &rsaquo;</a></div>"
		end
		return content
	end
end

# http://reganmian.net/blog/2013/10/02/blogging-with-nanoc-easy-workflow-for-embedding-images/
# class FixImages < Nanoc3::Filter
#   identifier :fix_images
#   def run(content, params={})
#     resp = content.gsub(/\!\[(.*?)\]\((.+?)\)/m) do
#       if $1[0..0] == ' ' && $1[-1..-1] == ' '
#         align = 'float:centre; margin-left: 10px; margin-right: 10px'
#       elsif $1[0..0] == ' '
#         align = "float:left; margin-right: 10px"
#       elsif $1[-1..-1] == ' '
#         align = 'float:right; margin-left: 10px'
#       else
#         align = 'none'
#       end
#       "<img src='#{$2}' style='#{align}'>"
#     end
#     resp.gsub(/\[((\<img).+?)\]\((.+?)\)/m, '<a href="\3">\1</a>')
#   end
# end

include PostHelper